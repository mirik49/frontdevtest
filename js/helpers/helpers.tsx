import Input from "../components/pieces/Input";
import React from "react";
import MobileDetect from "mobile-detect";

export const clearNumberField = (field) => {
    return field.replace(/[^-0-9]/gim, '');
};

export const renderData = (obj, className, first, second, third, onClick, params, firstText) => {
    return (Object.keys(obj).map((i, key) => {
        return (
            <li className={className + "__item"}
                onClick={() => params ? onClick(obj[i][params[0]], obj[i][params[1]]) : null}
                key={key}
            >
                <p className={className + "__text"}>{firstText ? firstText + " " + obj[i][first] : obj[i][first]}</p>
                <p className={className + "__text"}>{obj[i][second]}</p>
                <p className={className + "__text"}>{obj[i][third]}</p>
            </li>
        )
    }))

};

export const formatPhone = (projectPhone) => {
    return projectPhone.substring(0, 1) + " (" + projectPhone.substring(1, 4) + ") " + projectPhone.substring(4, 7) +
        "-" + projectPhone.substring(7, 9) + "-" + projectPhone.substring(9, 11);
};
