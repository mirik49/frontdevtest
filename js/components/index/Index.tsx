import React from "react";

import Pages from "../../layouts/Pages";
import "../../../scss/main.scss";
import Link from "next/link";

export default function Index() {

    return (
        <Pages>
            <div className="user-action__wrapper">
                <Link href={"/offices"}>
                    <a className="user-action__button">
                        Оплатить в офисе компании
                    </a>
                </Link>
            </div>
        </Pages>
    )
}
